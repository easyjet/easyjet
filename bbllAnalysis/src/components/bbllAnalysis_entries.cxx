#include "../BaselineVarsbbllAlg.h"
#include "../LeptonVarsbbllAlg.h"
#include "../BJetsVarsbbllAlg.h"
#include "../MMCDecoratorAlg.h"
#include "../HHbbllSelectorAlg.h"
#include "../NeutrinoWeightingAlg.h"
#include "../NeutrinoWeightingTool.h"
#include "../ResonantPNNbbllAlg.h"

using namespace HHBBLL;

DECLARE_COMPONENT(BaselineVarsbbllAlg)
DECLARE_COMPONENT(LeptonVarsbbllAlg)
DECLARE_COMPONENT(BJetsVarsbbllAlg)
DECLARE_COMPONENT(MMCDecoratorAlg)
DECLARE_COMPONENT(HHbbllSelectorAlg)
DECLARE_COMPONENT(NeutrinoWeightingAlg)
DECLARE_COMPONENT(NeutrinoWeightingTool)
DECLARE_COMPONENT(ResonantPNNbbllAlg)
